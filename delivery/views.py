from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from .producer import publish
from .serializer import DeliverySerializer
from .models import Delivery

class DeliveryView(APIView):
    def post(self, request):
        serializer = DeliverySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(
                {
                    "data": serializer.data
                },
                status=status.HTTP_201_CREATED
            )
        else:
            return Response(
                {
                    "error": serializer.errors
                },
                status=status.HTTP_400_BAD_REQUEST
            )
    
    def get(self, request, pk=None):
        if pk:
            delivery = get_object_or_404(Delivery, pk=pk)
            serializer = DeliverySerializer(delivery)
            return Response(
                {
                    "data": serializer.data,
                },
                status=status.HTTP_200_OK
            )
        deliveries = Delivery.objects.all()
        serializer = DeliverySerializer(deliveries, many=True)
        return Response(
            {
                "data": serializer.data
            },
            status=status.HTTP_200_OK
        )

    def put(self, request, pk=None):
        delivery = get_object_or_404(Delivery, pk=pk)
        serializer = DeliverySerializer(delivery, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(
                {
                    "data": serializer.data
                },
                status=status.HTTP_200_OK
            )
        else:
            return Response(
                {
                    "error": serializer.errors
                },
                status=status.HTTP_400_BAD_REQUEST
            )
    def delete(self, request, pk=None):
        delivery = get_object_or_404(Delivery, pk=pk)
        delivery.delete()
        return Response(
            {
                "message": "Item Deleted"
            },
            status=status.HTTP_200_OK
        )

@api_view(['GET'])
def produce(self):
    publish()
    return Response("Test")
