from django.urls import path
from .views import DeliveryView, produce
from . import views

app_name = 'delivery'
urlpatterns = [
    path('delivery', DeliveryView.as_view(), name='deliveryView'),
    path('delivery/<int:pk>', DeliveryView.as_view(), name='deliveryViewWithId'),
    path('produce', views.produce, name='produceMessage')
]
