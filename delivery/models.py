from django.db import models
from datetime import datetime

# Create your models here.
status = (
    ("Pesanan dijemput", "Pesanan dijemput"),
    ("Pesanan dikirim", "Pesanan dikirim"),
    ("Pesanan tiba", "Pesanan tiba")
)

class Delivery(models.Model):
    id = models.IntegerField(primary_key=True)
    status = models.CharField(
        max_length=255,
        choices=status,
        default="Pesanan dijemput"
    )
    created_at = models.DateTimeField(
        default=datetime.now()
    )
    delivered = models.BooleanField(
        default=False
    )

    def __str__(self) -> str:
        return '{} - {}'.format(self.id, self.status)
