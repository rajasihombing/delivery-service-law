from apscheduler.schedulers.background import BackgroundScheduler
from .models import Delivery
from datetime import datetime, timedelta


def siap_kirim():
    today = datetime.now().strftime('%H:%M:%S')
    orders = Delivery.objects.filter(delivered=False)
    for order in orders:
        ready_time = order.created_at + timedelta(minutes=1)
        if ready_time.strftime('%H:%M:%S') < today:
            order.status = 'Pesanan dikirim'
            order.save()

def kirim():
    today = datetime.now().strftime('%H:%M:%S')
    orders = Delivery.objects.filter(delivered=False, status='Pesanan dikirim')
    for order in orders:
        ready_time = order.created_at + timedelta(minutes=4)
        if ready_time.strftime('%H:%M:%S') < today:
            order.status = 'Pesanan tiba'
            order.delivered = True
            order.save()


def start():
    scheduler = BackgroundScheduler()
    scheduler.add_job(siap_kirim, "interval", minutes=1, id="order_001", replace_existing=True)
    scheduler.add_job(kirim, "interval", minutes=1, id="order_002", replace_existing=True)
    scheduler.start()
